#!/bin/sh

fail_exit() {
	echo "FAILED"
}
trap fail_exit EXIT

set -e

WPA_SERVICE="fi.w1.wpa_supplicant1"
WPA_OBJECT="/fi/w1/wpa_supplicant1"

# Just check if untrospection is available for object
test_new_supplicant() {
    echo connman-new-supplicant
    busctl --no-pager --system introspect "$WPA_SERVICE" "$WPA_OBJECT" >/dev/null 2>&1
}

test_new_supplicant

trap '' EXIT
echo "PASSED"
